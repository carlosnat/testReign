import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
 
@Injectable()
export class PostService {

    constructor(private _http:HttpClient){}

    get_all_posts(){
        return this._http.get('http://localhost:3000/api/hits');
    }

    delete_article(id){
        return this._http.delete('http://localhost:3000/api/hits',{params: {_id:id}});
    }
}