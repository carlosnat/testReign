import { Component, OnInit } from '@angular/core';

import { PostService } from '../service/post.service';
import { Item } from '../model/post';

import { DatePipe } from '@angular/common';


@Component({
    moduleId: module.id,
    selector: 'list-posts',
    templateUrl: 'listPosts.component.html',
    styleUrls: ['listPosts.component.css']
})
export class ListPostsComponents implements OnInit {
    constructor(private _postService: PostService) { }

    all_post:any;

    ngOnInit() {
        this.request_all_posts();    
    }

     request_all_posts(){
        this._postService.get_all_posts().subscribe( data => {
            let some_post:any = data;
            
            this.all_post = some_post.sort( (a: any, b: any) => {
                new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
            });
            console.log('sorted_list', this.all_post);
        });
     }

     delete_item(Item){
        console.log('item to delete', Item);
        this._postService.delete_article(Item._id).subscribe( res => {
            console.log('item deleted', res);
            this.request_all_posts();
        });
     }
}