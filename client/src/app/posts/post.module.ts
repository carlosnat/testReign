import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from '../posts/header/header.component';
import { ListPostsComponents } from '../posts/list/listPosts.components';

import { PostService } from '../posts/service/post.service';


@NgModule({
    declarations: [ListPostsComponents, HeaderComponent],
    imports: [ CommonModule ],
    exports: [
        ListPostsComponents, HeaderComponent
    ],
    providers: [
        PostService
    ],
})
export class PostsModule {}