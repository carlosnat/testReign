Welcome to TestResolve!
===================

Hello friends, here is my idea to solve the test proposed.
Like you said we have two main components, **Client** and **Server**.  The client was build with the Angular CLI, and the Server was build without scaffolding. 
Let's see what we have here.

> **Note:**

> - Clone the repo
```
git clone https://gitlab.com/carlosnat/testReign.git
``` 


----------
Server component
-------------
Open a console, cmd, bash in the server folder. You will need install the dependecys first.
```
npm install
```
after install all the dependecys you can run the server with the follow comand
```
nodemon server.js
```
with nodemon, if you do some chances the server restart automatically otherwise you can use:
```
npm start
```
> **Note:**

> - The server component need mongodb, be sure that you have installed in your computer.

----------
Client component
-------------

The client is build with angular cli, that help us a lot. open a cmd, console, bash pointing to client folder, and run:
```
ng serve
```

That command will install the dependencies and will publish the app in the address
```
localhost:4200
```

To prevent CORS problem, please install in chrome
`https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi`
