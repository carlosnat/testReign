'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HitSchema = new Schema({
    story_title: String,
    story_url: String,
    title: String,
    url: String,
    created_at: String
});

module.exports = mongoose.model('Hit', HitSchema);