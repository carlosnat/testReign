'use strict'

var cronJob = require('cron').CronJob;
var request = require('request');
var hitModel = require('../models/hitsModel');

module.exports = function(){
    var url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
    
    var jobs = new cronJob({
        cronTime:'1 * * * * *',
        onTick: function(){
            request(url, function (error, response, body) {

                //coment from this - to stop fetching data
                console.log('request posts to api');
                var hits = JSON.parse(body).hits;
                console.log('hits to save', hits.length);
                var promises = hits.map(function(hit){
                    if(hit.story_title && hit.story_title.length > 0){
                        return hitModel.create(hit);
                    }else if (hit.title && hit.title.length > 0){
                        return hitModel.create(hit);
                    }
                });
                Promise.all(promises).then(function(){
                    console.log('saved hits');
                });
                //coment to this - to stop fetching data
            
            })
        },
        start:false
    });
    jobs.start();
    /*new CronJob('* * * * * *', function() {
        console.log('You will see this message every second');
    }, null, true, 'America/Los_Angeles');*/
}