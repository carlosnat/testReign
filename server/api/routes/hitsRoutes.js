'use strict'

var hitsModel = require('../models/hitsModel');
var routes = require('./routes');

module.exports = function(app){
    routes(app, hitsModel, 'hits');
}