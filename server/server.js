var express = require('express'),
app = express(),

http = require('http'),
server = http.createServer(app),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/HitsDb', {useMongoClient:true}); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//middleware siempre sobre las rutas
app.use(function(req, res, next){
    //console.log(req.body);
    req.user = 'usuario logueado';
    next();
});

require('./api/routes/hitsRoutes')(app); //importing route
require('./api/jobs/getArticles')();

//app.listen(port);
server.listen(port);

console.log('todo list RESTful API server started on: ' + port);